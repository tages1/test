
NAMEIMAGE:= test:v1
NAMECONTAINER:= testcont

build:
	sudo docker build . -t $(NAMEIMAGE)

up:
	sudo docker run -d --rm \
	-v ./template:/usr/share/nginx/html \
	-v ./conf.d:/etc/nginx/conf.d \
	-p 8080:80 --name $(NAMECONTAINER) $(NAMEIMAGE)



uploadconf:
	sudo docker exec $(NAMECONTAINER) nginx -s reload


clean:
	sudo docker rmi $(sudo docker images | sed 's/\s\+/ /g' | cut -d ' ' -f3)