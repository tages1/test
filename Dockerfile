FROM nginx:1.25.1

COPY template/index.html /usr/share/nginx/html/

RUN useradd -M nginx; \
    chmod u-s \ 
    usr/bin/passwd \
    usr/bin/chfn \ 
    usr/bin/su \
    usr/bin/gpasswd \
    usr/bin/umount \
    usr/bin/newgrp \
    usr/bin/mount \
    usr/bin/chsh; \ 
    chmod g-s \
    usr/bin/wall \
    usr/bin/chage \
    usr/sbin/unix_chkpwd \
    usr/bin/expiry; \
    chown -R nginx:nginx /var/cache/nginx && \
    chown -R nginx:nginx /var/log/nginx && \
    chown -R nginx:nginx /etc/nginx/conf.d; \
    touch /var/run/nginx.pid && \
    chown -R nginx:nginx /var/run/nginx.pid; 

USER nginx


HEALTHCHECK --interval=3s --timeout=3s \
--start-period=5s --retries=3 CMD [ "curl", "localhost:80" ]

ENTRYPOINT [ "nginx" , "-g", "daemon off;" ]
