registry.gitlab.com/tages1/test это обычный  nginx образ, который запускаеться 
с базовыми параметрами 

*Примеры:*
    
    docker run -d --rm \
	-v ./template:/usr/share/nginx/html \
	-v ./conf.d:/etc/nginx/conf.d \
	-p 8080:80 --name NAMECONTAINER registry.gitlab.com/tages1/test

    Для обновление конфигурации образа можно использовать :
    docker exec NAMECONTAINER nginx -s reload

